#!/bin/bash

source configuration.file.txt

module load profile/advanced
module load autoload fastqc/0.11.3

echo "Performing Quality Control"
echo "`date`: Quality control "

echo -e "\nCreating output folders: QualityControl\n"

mkdir -p $QCON

echo "-------------------------------------------------------------------------"



for i in $SAMPLE_LIST; do
    echo "Sample $i..."

fastqc -o $wd/$PRJ_NAME/0_QualityControl --nogroup $INPUT_DIR/$i.R1.fastq $INPUT_DIR/$i.R2.fastq

    echo "---------------------------------------------------------------------"	
done
echo "-------------------------------------------------------------------------"
