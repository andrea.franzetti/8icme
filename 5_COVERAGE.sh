#! /bin/bash

source configuration.file.txt

echo "Performing Coverage"
echo "Parsing mapping output..."

echo "Creating output folder: Coverage Calculation"

mkdir -p $COV
echo "`date`: Calculating gene coverages"

echo "Importing samtools and bedtools"

module load profile/advanced
module load autoload samtools
module load autoload bedtools

for i in $SAMPLE_LIST; do
    
    echo "Samtools: production of bam file $i"
    samtools view -b -o $COV/bwt.idba.$i.bam $MAPP/bwt.idba.$i.sam -S
    echo "Samtools: bam sorting $i"
    samtools sort $COV/bwt.idba.$i.bam -o $COV/bwt.idba.$i.sort.bam
    echo "Bedtools: calculating coverage of $i"
    bedtools genomecov -ibam $COV/bwt.idba.$i.sort.bam -g $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta > $COV/coverage.$i.prodigal.bt
    echo "done"

    awk 'BEGIN {pc=""} 
    { 
        c=$1; 
        if (c == pc) { 
            cov=cov+$2*$5; 
        } else { 
            print pc,cov; 
            cov=$2*$5; 
        pc=c}
    } END {print pc,cov}' $COV/coverage.$i.prodigal.bt | tail -n +2 > $COV/coverage.$i.pergene
    
    z="id_gene $i"
    sed "1i $z"  $COV/coverage.$i.pergene > $COV/coverage.$i
done

echo "`date`: End of of all"

echo "-------------------------------------------------------------------------"
