#!/usr/bin/python

"""
Usage:

gencov.py [arg1] [arg2] [arg3] [cov1,cov2,...,covn] [output]


arg1: tabular file with metacyc RNX, metacyc pathways, EC numbers, UniProt ID and metacyc pathways ("RNX2PTW2path2ec2uniprt.tab")
arg2: output tabular file of kaiju with gene ID, NCBI taxomony ID and taxonomy path
arg3: tabular file from gene annotation with gen ID and Uniprot ID
cov: tabular files from mapping with gene ID and gene coverage
output: output file name

"""

    
import io
import sys
import string
import pandas as pd
import numpy as np


if len(sys.argv) == 1:
	print __doc__
	exit(0)



right=pd.read_table(sys.argv[1])
left=pd.read_table(sys.argv[3])
tax=pd.read_table(sys.argv[2])

m1=pd.merge(left, right, how='left', on='uniprot_id')

merge=pd.merge(tax, m1, how='left', on='id_gene')
temp=merge


for sam in sys.argv[4].split(','):
	sample=pd.read_table(sam, sep=' ')
	merge=pd.merge(merge, sample, how='left', on='id_gene')
	#merge.iloc[:,8:]=merge.iloc[:,8:].apply(lambda x: 100000*(x)/x.sum()) 
	
	#taxa=pd.merge(tax, sample, how='left', on='id_gene')
	#cla_taxa=taxa[taxa.cla.isin(['C'])]
		

	#cla_taxa.to_csv(sam+".tax.krona", sep='\t', index=False)
		
	#path=pd.merge(sample, temp, how='left', on='id_gene').iloc[:,[1,6,8]]
	#path[path.pathway.notnull()].to_csv(sam+".pathway.krona", sep='\t', index=False)
	#path.to_csv(sam+".pathway.krona", sep='\t', index=False)
	
merge.to_csv(sys.argv[5], sep='\t', index=False)

table=pd.read_table(sys.argv[5])
table[['RXN','PTW', 'pathway']] = table[['RXN','PTW', 'pathway']].fillna(value='NA')

pivot1=pd.pivot_table(table, index=['RXN','PTW','pathway'], aggfunc=np.sum, dropna='False')

pivot1.drop('ncbi_tax',1).to_csv(sys.argv[5]+'.pivot.rxn.tab', sep='\t')

pivot2=pd.pivot_table(pivot1, index=['PTW','pathway'], dropna='False', aggfunc=np.sum)

pivot2.drop('ncbi_tax',1).to_csv(sys.argv[5]+'.pivot.ptw.tab', sep='\t')

pivot3=pd.pivot_table(table[table.PTW != 'NA'], index=['RXN','PTW','pathway', 'ncbi_tax'], aggfunc=np.sum, dropna='False')

pivot4=pd.pivot_table(pivot3, index=['PTW','pathway','ncbi_tax'], dropna='False')


pivot4.to_csv(sys.argv[5]+'.pivot.ptw_tax.tab', sep='\t')


#stamp=pd.read_table(sys.argv[5]+'.pivot.rxn.tab')
#df1=stamp[['pathway','RXN']]
#df2=stamp.ix[:,3:]
#pd.concat([df1, df2], axis=1).to_csv(sys.argv[5]+'.pivot.rxn.stamp', sep='\t', index=False)

stamp=pd.read_table(sys.argv[5]+'.pivot.ptw.tab')
df1=stamp[['pathway']]
df2=stamp.ix[:,2:].fillna(value='0')
df3=pd.concat([df1, df2], axis=1).dropna(subset=['pathway'], how='any')
df3.to_csv(sys.argv[5]+'.pivot.ptw.stamp.txt', sep='\t', index=False)









