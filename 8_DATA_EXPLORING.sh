#! /bin/bash

source configuration.file.txt
echo -e "Importing Python/2.7.9 module"

module load autoload python/2.7.9
source env/bin/activate


echo -e "Create coverage table and Krona graph for thw whole communities or specific pathway"

read -p "Type pathway short name.. (type 'all' for the entire community) " ptw

python mining.py $OUT/final.table.$PRJ_NAME.tab.pivot.ptw_tax.tab $ptw

mkdir -p $KRON

m=4

for i in $SAMPLE_LIST; do

ktImportTaxonomy -o $ptw.$i.krona.html -t 3 -m $m  $ptw.ptw_tax.tab

m=$((m + 1))

mv $ptw.$i.krona.html $KRON

done

mv $ptw.ptw_tax.tab $KRON

