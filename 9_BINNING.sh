#! /bin/bash

source configuration.file.txt
module load autoload profile/advanced
module load autoload bowtie2
module load autoload python/2.7.9
module load autoload idba-ud

echo -e "Write file list"

file='list_file_maxbin.txt'

[ -f "$file" ] && rm $file

for i in $SAMPLE_LIST; do echo $FILT_READS/filtered.$i.R1.fastq >> list_file_maxbin.txt; echo $FILT_READS/filtered.$i.R2.fastq >> list_file_maxbin.txt; done

mkdir -p $BINN

mv list_file_maxbin.txt  $BINN

cd $BINN

echo -e "Run Binning of the co-Assembly with MaxBin"

run_MaxBin.pl -contig $ASSEMB_READS/d.$PRJ_NAME.idba/contig.fa -reads_list list_file_maxbin.txt -out bin.$PRJ_NAME



