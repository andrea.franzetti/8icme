#!/bin/bash

source configuration.file.txt
echo "Performing Mapping"

echo "Create output folder: Mapping"
mkdir -p $MAPP

echo "Mapping reads against genes.."

echo "`date`: Building database"

echo "Importing Bowtie"
module load autoload profile/advanced
module load autoload bowtie2
module load autoload python/2.7.9

bowtie2-build $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta $MAPP/bwt.idba.$PRJ_NAME

echo "`date`: Starting Bowtie2"

for i in $SAMPLE_LIST; do
    
    bowtie2 -x $MAPP/bwt.idba.$PRJ_NAME \
	    -1 $FILT_READS/filtered.$i.R1.fastq \
	    -2 $FILT_READS/filtered.$i.R2.fastq \
	    -U $FILT_READS/filtered.single.$i.fastq \
	    -S $MAPP/bwt.idba.$i.sam

done

echo "`date`: End of Bowtie2"


echo "-------------------------------------------------------------------------"
