#!/bin/bash

source configuration.file.txt
echo "Performing Gene Pred"

echo "Create output folder: Gene Prediction"
mkdir -p $GENE_PRED

echo "Gene prediction with Prodigal...."

echo -e "\n `date`: Running Prodigal..."


#$LNBIN/prodigal -i $ASSEMB_READS/d.$PRJ_NAME.idba/contig.fa \
prodigal -i $ASSEMB_READS/d.$PRJ_NAME.idba/contig.fa \
    -d $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta \
    -a $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
    -o $GENE_PRED/prodigal.idba.$PRJ_NAME.gbk \
    -p meta

echo -e "\n `date`: Running Annotation with Diamond..."


#-f in diamond blastp 0.7 is the format to be used
#$LNBIN/diamond blastp --query $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
diamond blastp --query $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
	       --db $DB/uniprot \
	       -o $GENE_PRED/diamond.prodigal.idba.$PRJ_NAME.m8 \
	       --sensitive

echo -e "`date`: Diamond Ended \n"
echo "Parsing Diamond output..."

sort -u -k1,1 $GENE_PRED/diamond.prodigal.idba.$PRJ_NAME.m8 > $GENE_PRED/sort.best.diamond.prodigal.idba.$PRJ_NAME.m8

paste <(cut -f1 $GENE_PRED/sort.best.diamond.prodigal.idba.$PRJ_NAME.m8) \
      <(cut  -f2 -d '|' $GENE_PRED/sort.best.diamond.prodigal.idba.$PRJ_NAME.m8) \
      > $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab

grep -v '@' $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab > $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab.temp
grep -v '*' $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab.temp > $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab

rm $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab.temp



z='id_gene\tuniprot_id'
sed "1i $z" $GENE_PRED/geneID2uniprot.$PRJ_NAME.tab > $GENE_PRED/id_gene2uniprot.$PRJ_NAME.tab

echo -e  "`date`: Diamond output parsing Ended \n "
echo "-------------------------------------------------------------------------"
