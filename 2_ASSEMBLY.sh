#!/bin/bash

source configuration.file.txt

module load autoload profile/advanced
module load autoload idba-ud

echo "Performing Assembly"
echo "Create output folder: Assembly"

module load autoload idba-ud

mkdir -p $ASSEMB_READS

echo -e  "Co-assembly....\n"
echo "Preparing files for Idba_ud "


cat $FILT_READS/filtered*R1*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq
cat $FILT_READS/filtered*R2*fastq > $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq
module load autoload idba-ud/1.1.1

#convertire fastq in fasta
fq2fa --merge $ASSEMB_READS/cat.filtered.$PRJ_NAME.R1.fastq \
    $ASSEMB_READS/cat.filtered.$PRJ_NAME.R2.fastq \
    $ASSEMB_READS/interleaved.cat.filtered.$PRJ_NAME.fasta

echo "`date`: Idba started "

idba_ud -r $ASSEMB_READS/interleaved.cat.filtered.$PRJ_NAME.fasta \
    -o $ASSEMB_READS/d.$PRJ_NAME.idba \
    --mink $MIN_KMER_LEN \
    --maxk $MAX_KMER_LEN \
    --step $STEP \
    --pre_correction \
    --num_threads 5

echo "`date`: Idba Ended"

echo "-------------------------------------------------------------------------"
