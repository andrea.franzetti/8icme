#! /bin/bash

source configuration.file.txt

echo "Importing modules"

module load profile/advanced

list=${SAMPLE_LIST// /,coverage.}
list="coverage.$list"

echo -e "Importing Python/2.7.9 module"

module load autoload python/2.7.9
source $wd/env/bin/activate

mkdir -p $OUT
cd $PRJ_NAME/5_Coverages/

python $wd/gencov.py $DB/RNX2PTW2path2ec2uniprt.tab \
       $TAX/id_gene2tax.tab \
       $GENE_PRED/id_gene2uniprot.$PRJ_NAME.tab \
       $list $OUT/final.table.$PRJ_NAME.tab

#ktImportTaxonomy -o $TAX/krona.taxa.$PRJ_NAME.html $TAX/*.tax.krona -m 3
#ktImportText $TAX/*pathway.krona -o $TAX/krona.metacyc.$PRJ_NAME.html
