#! /bin/bash

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

echo -e "\n This script will install the dependencies necessary for the
metagenomic pipeline.\n"

echo -e "PLEASE NOTE THAT THIS SCRIPT HAS BEEN WRITTEN IN APRIL 2017\n"
echo -e "To install more recent version, pleas edit the script\n"

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"

#================== SETTIN ENV  ===================
echo -e "\n Creating a deps folder in the working directory ($(pwd))"

wd=$(pwd)

DEPS=$(pwd)/deps
mkdir -p $DEPS

cd $DEPS

echo -e "\n Creating a folder that will contain sl to binaries \n"

SLBIN=$DEPS/slbin
echo $SLBIN
mkdir -p $SLBIN

echo -e "\n Creating a folder that will contain the tar arichives \n"

TAR=$DEPS/archives
echo $TAR
mkdir -p $TAR

echo -e "================== SICKLE ===================\n"
echo -e "I'm going to install the following software: -sickle version 1.33"

wget -O sickle_1.3.3.tar.gz https://github.com/najoshi/sickle/archive/v1.33.tar.gz
tar -zxvf sickle_1.3.3.tar.gz
cd sickle-1.33
make

ln -s $(pwd)/sickle $SLBIN/sickle

cd ..

mv $DEPS/sickle_1.3.3.tar.gz $TAR

echo -e "================== PRODIGAL 2.6.3 ===================\n"
echo -e "I'm going to install the following software: Prodigal2.6.3"

wget -O Prodigal_2.6.3.tar.gz https://github.com/hyattpd/Prodigal/archive/v2.6.3.tar.gz
tar -zxvf Prodigal_2.6.3.tar.gz

cd Prodigal-2.6.3

make

ln -s $(pwd)/prodigal $SLBIN/prodigal

cd ..

mv $DEPS/Prodigal_2.6.3.tar.gz $TAR

echo -e "================== DIAMOND 0.8.36 ===================\n"
echo -e "I'm going to install the following software: Diamond version 0.8.36"

wget -O diamond_0.8.36.tar.gz  https://github.com/bbuchfink/diamond/releases/download/v0.8.36/diamond-linux64.tar.gz
mkdir -p $DEPS/diamond
mv diamond_0.8.36.tar.gz $DEPS/diamond

cd diamond
tar -zxvf diamond_0.8.36.tar.gz

ln -s $(pwd)/diamond $SLBIN/diamond

cd ..


echo -e "================== KAIJU 0.8.36 ===================\n"
echo -e "I'm going to install the following software: Kaiju version 1.5.0"

wget -O kaiju_1.5.0.tar.gz https://github.com/bioinformatics-centre/kaiju/releases/download/v1.5.0/kaiju-1.5.0-linux-x86_64.tar.gz
tar -zxvf kaiju_1.5.0.tar.gz
ln -s $DEPS/kaiju-v1.5.0-linux-x86_64-static/bin/kaiju $SLBIN/kaiju

mv $DEPS/kaiju_1.5.0.tar.gz $TAR

echo -e "================== KRONA 2-7 ===================\n"
echo -e "I'm going to install the following software: Krona"
wget -O KronaTools-2.7.tar https://github.com/marbl/Krona/releases/download/v2.7/KronaTools-2.7.tar

tar -xvf KronaTools-2.7.tar
mv *tar $TAR
cd KronaTools-2.7
mkdir bin
./install.pl --prefix ./
./updateTaxonomy.sh
#./updateAccessions.sh

cd $DEPS

ln -s $DEPS/KronaTools-2.7/bin $SLBIN/Krona

echo -e "================== Maxbin ===================\n"
echo -e "I'm going to install Maxbin and its dependecies: Frag Gene Scan and hmmer "

wget -O MaxBin.tar.gz --no-check-certificate https://downloads.sourceforge.net/project/maxbin/MaxBin-2.2.3.tar.gz
wget -O hmmer.tar.gz http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz
wget -O FragGeneScan.tar.gz https://github.com/wltrimbl/FGS/archive/FGS1.18.tar.gz

tar -zxvf MaxBin.tar.gz
tar -zxvf hmmer.tar.gz
tar -zxvf FragGeneScan.tar.gz

mv *.tar.gz $TAR

echo -e "---Building FragGeneScan---"
mv FGS-FGS1.18 fgs

cd fgs
make clean
make fgs
cd $DEPS
 
ln -s $DEPS/fgs $SLBIN/fgs

echo -e "---Building hmmer---"
mv hmmer-3.1b2-linux-intel-x86_64 hmmer

cd hmmer
./configure
make
make check
cd $DEPS
ln -s $DEPS/hmmer/binaries $SLBIN/hmmer

echo -e "---Building MaxBin---"

mv MaxBin-2.2.3 MaxBin

cd MaxBin/src/

make

cd $DEPS

ln -s $DEPS/MaxBin $SLBIN/MaxBin
echo -e "---Importing Modules---"

module load autoload idba-ud bowtie2


#==== python scripts ====

echo -e "---Setting assemstats2.py---"

cd $DEPS

cd ../

module load autoload git
module load autoload python/2.7.9
echo $(pwd)

echo -e "Creating virtualenv 'venv' \n"

virtualenv -p python env

echo -e "Sourcing and cloning repo"
source env/bin/activate
git clone git://github.com/acr/screed.git

cd screed

echo -e "Installing Screed"

python setup.py install

echo -e "Installing pandas in the virtualenv"
pip install pandas

deactivate

cd $wd 

echo -e "Downloading 'assemstats2.py'"

wget --no-check-certificate https://raw.githubusercontent.com/Victorian-Bioinformatics-Consortium/khmer/master/sandbox/assemstats2.py

cd $wd
echo -e "================== FINISH! ===================\n"

echo -e "Now you have al the dipendecies to run on PICO"
