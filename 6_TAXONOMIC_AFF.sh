#! /bin/bash

source configuration.file.txt
source env/bin/activate

echo "Performing Taxonomy Affiliation"
echo "Assigning taxonomy to genes with kaiju..."

echo "Creating output folder: Taxonomy Affiliation"
mkdir -p $TAX
echo "`date`: kaiju started"
#$LNBIN/kaiju -t $DB/nodes.dmp \
kaiju -t $DB/nodes.dmp \
    -f $DB/kaiju_db.fmi \
    -i $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta \
    -o $TAX/taxa.prodigal.idba.$PRJ_NAME.tab

z="cla\tid_gene\tncbi_tax"
sed "1i $z"  $TAX/taxa.prodigal.idba.$PRJ_NAME.tab > $TAX/id_gene2tax.tab


echo "`date`: kaiju ended"

